
abstract class CustomerProfile{
  bool isLoyalCustomer();
}

class HealthInsurance implements CustomerProfile{
  @override
  bool isLoyalCustomer() {
    return true;
  }
}

class VehicleInsurance implements CustomerProfile{
  @override
  bool isLoyalCustomer() {
    return true;
  }
}

class HomeInsurance implements CustomerProfile{
  @override
  bool isLoyalCustomer() {
    return true;
  }
}

class InsurancePremiumDiscountCalculator {
  int calculate(CustomerProfile customer){
    if (customer.isLoyalCustomer()){
      return 20;
    }
    return 0;
  }
}


class DemoOpenClosedPrinciple1{
  static void main(){

  }
}
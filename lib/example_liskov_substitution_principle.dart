
class Rectangle{
  int? width, height;

  Rectangle({this.width, this.height});

  int getWidth(){
    return width ?? 0;
  }

  void setWidth(int width){
    this.width = width;
  }

  int getHeight(){
    return height ?? 0;
  }

  void setHeight(int height){
    this.height = height;
  }

  int getArea(){
    return (width ?? 0)*(height ?? 0);
  }

  @override
  String toString() {
    String str = "Rectangle{width = $width, height = $height";
    return str;
  }
}

class Square extends Rectangle{
  Square(int size): super(width: size, height: size);

  @override
  void setWidth(int width) {
    // TODO: implement setWidth
    super.setWidth(width);
    super.setHeight(width);
  }

  @override
  void setHeight(int height) {
    // TODO: implement setHeight
    super.setHeight(height);
    super.setWidth(height);
  }
}

// Liskov substitution principle
class RectangleFactory{
  static Rectangle newRectangle(int width, int height){
    return Rectangle(width: width, height: height);
  }

  static Rectangle newSquare(int side){
    return new Rectangle(width: side, height: side);
  }
}

class DemoLiskoSubstitutionPrinciple{
  static void useIt(Rectangle re){
    int width = re.getWidth();
    re.setHeight(10);
    print("Expected area of ${width*10}, got ${re.getArea()}");
  }

  static void main(){
    Rectangle rc = Rectangle(width: 2, height: 3);
    useIt(rc);

    Rectangle sq = Square(5);
    useIt(sq);


  }
}
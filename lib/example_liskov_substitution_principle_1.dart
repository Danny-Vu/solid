
class Product{
  int discount = 20;

  int getDiscount(){
    return discount;
  }
}

class InHouseProduct extends Product{

  // apply Liskov here

  @override
  int getDiscount() {
    applyExtraDiscount();
    return super.getDiscount();
  }

  void applyExtraDiscount(){
    discount = discount * 2;
  }
}

class DemoLiskevSubstitutionPrinciple1{
  static void main(){
    Product p1 = Product();
    Product p2 = Product();
    Product p3 = InHouseProduct();
    List<Product> _list = [];
    _list.insertAll(0, [p1, p2, p3]);

    _list.forEach((element) => print(element.getDiscount()));
  }
}
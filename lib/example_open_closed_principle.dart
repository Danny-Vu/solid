enum Color { Red, Green , Blue}
enum Size { Small, Medium, Large, Yuge}

class Product{
  String? name;
  Color? color;
  Size? size;

  Product({this.name, this.color, this.size});
}

class ProductFilter {
  Future<List<Product>> filterByColor(List<Product> products, Color color) async{
    return await products.where((p) => p.color == color).toList();
  }

  Future<List<Product>> filterBySize(List<Product> products, Size size) async{
    return await products.where((p) => p.size == size).toList();
  }

  Future<List<Product>> filterBySizeAndColor(List<Product> products, Size size, Color color) async{
    return await products.where((p) => p.size == size && p.color == color).toList();
  }
}


// Open closed principle
abstract class Specification<T>{
  bool isSatisfied(T item);
}

abstract class Filter<T>{
  Future<List<T>> filter(List<T> items, Specification<T> spec);
}

class ColorSpecification implements Specification<Product>{
  Color? color;
  ColorSpecification({this.color});

  @override
  bool isSatisfied(Product item){
    return item.color == color;
  }
}

class SizeSpecification implements Specification<Product>{
  Size? size;
  SizeSpecification({this.size});

  @override
  bool isSatisfied(Product item){
    return item.size == size;
  }
}

class BetterFilter implements Filter<Product>{

  @override
  Future<List<Product>> filter(List<Product> items, Specification<Product> spec) async{
    List<Product> products = await items.where((item) => spec.isSatisfied(item)).toList();
    return  products;
  }
}

class AndSpecification<T> implements Specification<T>{
  Specification<T>? first, second;

  AndSpecification({this.first,this.second});

  @override
  bool isSatisfied(T item){
    return first!.isSatisfied(item) && second!.isSatisfied(item);
  }
}

class DemoOpenClosePrinciple{
  static void main() async{
    Product apple =   Product(name:"Apple",color:Color.Green, size:Size.Small);
    Product tree  =   Product(name:"Tree", color:Color.Green, size:Size.Large);
    Product house =   Product(name:"House",color: Color.Blue, size:Size.Large);

    List<Product> products = [];
    products.insertAll(0,[apple, tree, house]);

    ProductFilter pf = ProductFilter();
    print("Green products (old):");
    List<Product> fp = await pf.filterByColor(products, Color.Green);
    fp.forEach((fp) => print("${fp.name} is green"));

    // Open Closed Principle
    BetterFilter bf = BetterFilter();
    print("Green products (new):");
    List<Product> fb = await bf.filter(products, ColorSpecification(color: Color.Green));
    fb.forEach((fb) => print("${fb.name} is green"));

    print("Large blue products (new):");
    List<Product> all = await bf.filter(products,
        AndSpecification(first: ColorSpecification(color:Color.Blue),
            second: SizeSpecification(size:Size.Large))
    );
    all.forEach((all) => print("${all.name} is large and blue"));
  }
}
import 'package:flutter/material.dart';
import 'package:solid/example_liskov_substitution_principle.dart';
import 'example_liskov_substitution_principle_1.dart';
import 'example_open_closed_principle.dart';


void main() {
  // Open closed principle
  //DemoOpenClosePrinciple.main();

  // Liskiv substitution principle
  //DemoLiskoSubstitutionPrinciple.main();

  // Liskiv substitution principle 1
  DemoLiskevSubstitutionPrinciple1.main();
}




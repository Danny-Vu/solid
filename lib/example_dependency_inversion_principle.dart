
// High Level
class NormalProductCatelog{
  void listAllProducts(){
    NormalSQLProductRepository _normalSQL = NormalSQLProductRepository();
    List<String> allProductNames = _normalSQL.getAllProductNames();
  }
}

// Low Level
class NormalSQLProductRepository{
  List<String> getAllProductNames(){
    return ["apple", "water melon"];
  }
}

// Dependency inversion principle

abstract class ProductRespository {
  List<String> getAllProductNames();
}

class SQLProductRepository implements ProductRespository{

  @override
  List<String> getAllProductNames() {
    return ["apple", "water melon"];
  }
}




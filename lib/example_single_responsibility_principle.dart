class NormalSquare {
  int side = 5;

  int getArea() {
    return side * side;
  }

  int getPerimeter() {
    return side * 4;
  }

  void draw() {}

  void rotate(int degree) {}
}

class NormalStudent {
  String? studentId;
  DateTime? studentDOB;
  String? address;

  void save() {
    // save on their own database
  }

  String getStudentId() {
    return studentId ?? '';
  }
  void setStudentId(String studentId){
    studentId = studentId;
  }
}

// Single responsibility principle (cohesion and coupling)

// cohesion
class Square {
  int side = 5;

  int getArea() {
    return side * side;
  }

  int getPerimeter() {
    return side * 4;
  }
}

class SquareUI {
  void draw() {}

  void rotate(int degree) {}
}

//coupling
class Student {
  String? studentId;
  DateTime? studentDOB;
  String? address;

  void save() {
    StudentRepository.save(this);
  }

  String getStudentId() {
    return studentId ?? '';
  }
  void setStudentId(String studentId){
    studentId = studentId;
  }
}
class StudentRepository{
  static void save(Student student){
    // we can change everything later
  }
}
